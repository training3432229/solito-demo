import Link from 'next/link'
import React from 'react'

export default function Demo() {
  return (
    <div>
        <Link href={'/new-page'}>
            New page
        </Link>
    </div>
  )
}
