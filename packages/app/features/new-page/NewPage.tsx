import { View, Text, useResponsiveValue } from 'dripsy'
import React from 'react'

const NewPage = () => {
  const resolution = useResponsiveValue([
    'small resolution',
    'medium small resolution',
    'medium large resolution',
    'large resolution',
  ])
  return (
    <View
      sx={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: ['red', 'white', 'blue', 'green'],
      }}
    >
      <Text>{resolution}</Text>
    </View>
  )
}

export default NewPage